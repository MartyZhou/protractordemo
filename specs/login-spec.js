var regeneratorRuntime = require('regenerator-runtime');

describe('login seismic', () => {
    beforeAll(async () => {
        browser.ignoreSynchronization = true;
        await browser.get('https://qafeature1.seismic.com');
    });

    it('should login', async () => {
        await element(by.id('TextBoxUserName')).sendKeys('admin');
        await element(by.id('TextBoxPassword')).sendKeys('Seismic!23');

        await element(by.id('ContentPlaceHolderBody_btnLogin')).click();

        browser.explore();

        await browser.sleep(30000);
    });
});