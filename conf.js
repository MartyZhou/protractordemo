require('babel-core/register');

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    seleniumServerStartTimeout: 360000,
    jasmineNodeOpts: {defaultTimeoutInterval: 360000},
    specs: ['specs/profile/profile-basic-spec.js']
};