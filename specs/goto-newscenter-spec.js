var regeneratorRuntime = require('regenerator-runtime');

describe('navigate to NewsCenter', () => {
    beforeAll(async () => {
        // before all logic
    });

    it('should navigate to NewsCenter', async () => {
        let newsCenterLink = await element(by.linkText('NewsCenter'));
        await newsCenterLink.click();

        await browser.sleep(15000);

        let titleHeader = await element(by.css('.seis-newscenter-page-header'));
        let titleSpan = await titleHeader.element(by.css('.ellipsis-text'));

        expect(await titleSpan.getText()).toEqual('COVER STORIES');

        await browser.sleep(2000);
    });
});