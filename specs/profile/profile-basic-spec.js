var regeneratorRuntime = require('regenerator-runtime');

describe('profile basic tests', () => {
    beforeAll(async () => {
        browser.ignoreSynchronization = true;
        await browser.get('http://localhost:8080/');
    });

    it('create new profile', async () => {
        await browser.sleep(1000);

        const profileHeader = await element(by.className('seis-pb-profile-header'));
        const headerButtons = await profileHeader.all(by.tagName('button'));
        browser.explore();
        const addButton = await headerButtons.get(0);
        await addButton.click();
        
        browser.explore();
    });
});